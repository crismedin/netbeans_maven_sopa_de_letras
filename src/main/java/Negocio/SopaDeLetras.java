package Negocio;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.hssf.usermodel.*;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];
    private char sopas2[][];
    private String imprimir="";
    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

//    
//    
//    public void leerExcel(String ruta) throws Exception {
//        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(ruta));
//        //Obtiene la hoja 1
//        HSSFSheet hoja = archivoExcel.getSheetAt(0);
//        //Obtiene el número de la última fila con datos de la hoja.
//        int canFilas = hoja.getLastRowNum()+1;
//        System.out.println("Filas:"+canFilas);
//        for (int i = 0; i < canFilas; i++) {
//            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
//            HSSFRow filas = hoja.getRow(i);
//            //Obtiene la cantidad de colomunas de esa fila
//            int cantCol=filas.getLastCellNum();
//            
//        for(int j=0;j<cantCol;j++)    
//        {
//            //Obtiene la celda y su valor respectivo
//            //double r=filas.getCell(i).getNumericCellValue();
//            String valor=filas.getCell(j).getStringCellValue();
//            System.out.print(valor+"\t");
//        }
//     System.out.println();
//       }
//    }
    //llenar la sopa
    public SopaDeLetras(String ruta)throws Exception{
      if(ruta.isEmpty()){
      throw new Exception("la ruta no es valida");
      }
    //creo el objeto sopa excel
        HSSFWorkbook sopaExcel = new HSSFWorkbook(new FileInputStream(ruta));
        //obtengo la hoja 1
        HSSFSheet hoja = sopaExcel.getSheetAt(0);
        this.sopas = new char[hoja.getLastRowNum()+1][];
        this.sopas2 = new char[hoja.getLastRowNum()+1][];
        //recorro las filas y las envio para armar la sopa
      
        for(int i = 0; i<hoja.getLastRowNum()+1; i++){
            //convierto la fila en un array char
            HSSFRow fila = hoja.getRow(i); 
            //le doy tamaño a la fila i
            this.sopas2[i] = new char[fila.getLastCellNum()];
            this.sopas[i] = new char[fila.getLastCellNum()];
            //paso la fila a sopa, le envio la fila y el numero de la fila a copiar
            llenarFilaDeSopa(fila,i);
        
        } // this.copia = this.sopas;
    
    }
    public void llenarFilaDeSopa(HSSFRow fila, int numFila){
        String palabraFila = "";
        //convierto el hssfrow en un string
        for(int i = 0; i < fila.getLastCellNum(); i++){
            palabraFila = palabraFila + fila.getCell(i);
        }
        //Convierto la fila en un arreglo
        char [] arregloFila = palabraFila.toCharArray();
        
        for(int j = 0; j<sopas[numFila].length; j++){
            sopas[numFila][j] = arregloFila[j];
            sopas2[numFila][j] = arregloFila[j];
        }
        
    }
    
    public void crearPDF(String ruta)throws Exception{
    if(ruta.isEmpty()){throw new Exception("Error no se puede cargar la sopa");}
    Document document = new Document();

    PdfWriter.getInstance(document, new FileOutputStream(ruta));
    document.open();

    PdfPTable table = new PdfPTable(sopas[0].length);
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                PdfPCell celda = new PdfPCell(new Phrase(String.valueOf(sopas[i][j])));
                celda.setHorizontalAlignment(1);//para que la letra qquede en la mitad :v
                celda.setPadding(5); //el alto de la celda
                if(sopas2[i][j]==2){celda.setBackgroundColor(BaseColor.ORANGE);}
                if(sopas2[i][j]==1){celda.setBackgroundColor(BaseColor.GREEN);}//pintar
                if(sopas2[i][j]==3){celda.setBackgroundColor(BaseColor.CYAN);}
                if(sopas2[i][j]==4){celda.setBackgroundColor(BaseColor.YELLOW);}
                table.addCell(celda);   //añadir la celda a la tabla}}
            
                
    
            }
        } document.add(table);
          document.close();
        
    }
  
    
    public void busquedaPrimera(String palabra)throws Exception{
        if(palabra.isEmpty() || palabra.length()>sopas[0].length || palabra.length()>sopas.length){System.err.println("la palabra no es valida");}
         for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (palabra.charAt(0)==sopas[i][j]) {
                         direccion(i,j,palabra);}
                 }}  }
    
    public void direccion(int fila, int columna, String palabra){
         
         boolean hoAdelante = (columna+1<sopas[fila].length) && palabra.charAt(1)==sopas[fila][columna+1];
         boolean hoAtras = (columna-1>=0) && palabra.charAt(1)==sopas[fila][columna-1];
         boolean veAbajo = (fila+1<sopas.length) && palabra.charAt(1)==sopas[fila+1][columna];
         boolean veArriba = (fila-1 >=0) && palabra.charAt(1)==sopas[fila-1][columna];
         boolean derechaDiagAbajo = (fila+1<sopas.length && columna-1>=0) && palabra.charAt(1)==sopas[fila+1][columna-1];
         boolean derechaDiagArriba = (fila-1 >=0 && columna+1<sopas[fila].length)&& palabra.charAt(1)==sopas[fila-1][columna+1];
         boolean izquierdaDiagAbajo = (fila+1<sopas.length && columna+1<sopas[fila].length) && palabra.charAt(1)==sopas[fila+1][columna+1];
         boolean izquierdaDiagArriba = (fila-1 >=0 && columna-1>=0) && palabra.charAt(1)==sopas[fila-1][columna-1];
         //buscar horizontal
         if (hoAdelante ||  hoAtras && palabra.length()<=sopas[0].length) {
             busquedaHorizontal(palabra,fila,columna,hoAdelante,hoAtras);
         }//buscar vertical
         if( veAbajo || veArriba && palabra.length()<=sopas.length){
             busquedaVertical(palabra,fila,columna,veAbajo,veArriba);
         }//buscar derecha diagonal
         if( (derechaDiagAbajo || derechaDiagArriba) ){
             busquedaDiagonalDerecha(palabra,fila,columna,derechaDiagArriba,derechaDiagAbajo);
         }//buscar izquierda diagonal
         if((izquierdaDiagAbajo || izquierdaDiagArriba) ){
             busquedaDiagonalIzquierda(palabra,fila,columna,izquierdaDiagArriba,izquierdaDiagAbajo);
         }
         
     }
    
    public void busquedaDiagonalDerecha(String palabra, int fila, int columna, boolean arriba, boolean abajo)
    {   
        String palabraAux = ""; boolean s= false;  boolean s2= false; String palabraAux2 = "";
        int x=columna;     int x2 = columna;
        int y=fila;        int y2 = fila;
        if(arriba && y>=palabra.length()-1 && (sopas[0].length-x)>=palabra.length()){
            for (int i = 0; i <palabra.length(); i++)
            {
            palabraAux=palabraAux+sopas[y][x];
            
            if(s){this.sopas2[y][x] = 2;}
            if(palabra.equals(palabraAux)&&!s){
                s = true; i = -1; palabraAux="";y=fila+1; x=columna-1;
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" de forma diagonal derecha arriba");}
            y--; x++;
            }
        }
       //  palabraAux = ""; s = false;
        if (abajo && x2>=palabra.length()-1 && (sopas.length-y2)>=palabra.length()){
            for (int j = 0; j < palabra.length(); j++) 
            {
                palabraAux2=palabraAux2+sopas[y2][x2];
                
                if(s2)this.sopas2[y2][x2] = 2;
                if(palabra.equals(palabraAux2)&&!s2){
                s2 = true; j = -1; palabraAux2="";y2=fila-1; x2=columna+1;
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" de forma diagonal derecha abajo");}
                 y2++; x2--;
        }
            }
            
        
    }
    
     public void busquedaDiagonalIzquierda(String palabra, int fila, int columna, boolean arriba, boolean abajo)
    {   
        String palabraAux = ""; boolean s= false;   String palabraAux2 = ""; boolean s2= false;
        int x=columna;      int x2= columna;
        int y=fila;         int y2= fila;
        if(arriba && fila>=sopas.length-1 && columna>=sopas.length-1){
            for (int i = 0; i <palabra.length(); i++){
            palabraAux=palabraAux+sopas[y][x];
            
            if(s){this.sopas2[y][x] = 3;}
            if(palabra.equals(palabraAux)&&!s){
                s = true; i = -1; palabraAux="";y=fila+1; x=columna+1;
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" de forma diagonal izquierda arriba");}
            y--; x--;
            }
        }
        if (abajo && sopas[0].length-x2>=palabra.length() && (sopas.length-y2)>=palabra.length()){
            for (int i = 0; i < palabra.length(); i++) 
            {   
                //System.out.print(sopas[y2][x2]);
                palabraAux2=palabraAux2+sopas[y2][x2];
                
                if(s2)this.sopas2[y2][x2] = 3;
                if(palabra.equals(palabraAux2)&&!s2){
                s2 = true; i = -1; palabraAux2="";y2=fila-1; x2=columna-1;
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" de forma diagonal izquierda abajo");}
                 y2++; x2++;
        } 
            }
            
        
    }
     
     public void busquedaVertical(String palabra, int fila, int columna,boolean veAbajo,boolean veArriba){
     
     String palabraAuxiliar="";   boolean s= false;  boolean s2 = false;    String palabraAuxiliar2="";
        if(veArriba){ 
         for (int i = fila; palabraAuxiliar.length()!=palabra.length() && fila>=palabra.length()-1; i--) {
             
             palabraAuxiliar = palabraAuxiliar + sopas[i][columna];
             if(s)this.sopas2[i][columna] = 4;
             if(palabra.equals(palabraAuxiliar) && !s){
                s = true; i = fila+1; palabraAuxiliar="";
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" verticalmente hacia arriba");
             }
         }
        } 
        //si no es igual y llego al length
        //
        if(veAbajo && (sopas.length-fila)>=palabra.length()-1 ){
            for (int i = fila; palabraAuxiliar2.length()!=palabra.length(); i++) {
                palabraAuxiliar2 = palabraAuxiliar2 + sopas[i][columna]; 
               if(s2)this.sopas2[i][columna] = 4;
            if(palabra.equals(palabraAuxiliar2) && !s2){
            s2 = true; i = fila-1; palabraAuxiliar2="";
            setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" verticalmente hacia abajo");}

            
            }
        
        }
     }
    
    public void busquedaHorizontal(String palabra, int fila, int columna,boolean hoAdelante,boolean hoAtras){
       String palabraAuxiliar="";   boolean s= false;  boolean s2 = false;    String palabraAuxiliar2=""; 
       char filaBusqueda[] = sopas[fila];   
        if(hoAdelante){ 
         for (int i = columna; palabraAuxiliar.length()!=palabra.length() && filaBusqueda.length-columna>=palabra.length(); i++) {
             
             palabraAuxiliar = palabraAuxiliar + filaBusqueda[i];
             if(s)this.sopas2[fila][i] = 1;
             if(palabra.equals(palabraAuxiliar) && !s){
                s = true; i = columna-1; palabraAuxiliar="";
                setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" horizontalmente hacia adelante");
             }
         }
        
        } 
        //si no es igual y llego al length
        //
        if(hoAtras && columna>=palabra.length()-1){
            for (int i = columna; palabraAuxiliar2.length()!=palabra.length(); i--) {
                palabraAuxiliar2 = palabraAuxiliar2 + filaBusqueda[i]; 
               if(s2)this.sopas2[fila][i] = 1;
            if(palabra.equals(palabraAuxiliar2) && !s2){
            s2 = true; i = columna+1; palabraAuxiliar2="";
            setImprimir("se encontro en la fila "+(fila+1)+" columna "+(columna+1)+" horizontalmente hacia atras");}

            
            }
        
        }
    
    }
    
    @Override
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    public boolean esPalindromo(String palabra) {
        String palabra2 = palabra;  int contador = 0;
        int j = 0;
        for (int i = palabra.length()-1; i >=0; i--) {
            if(palabra2.charAt(i)==palabra.charAt(j)){
               contador++;
            }
            j++;}
        return contador==palabra.length();
    }
    
    public boolean esCuadrada() 
    {  
        boolean rta=true;
        if(getSopas()==null){return false;}
        for(int i = 0; i<this.sopas.length && rta; i++){
            if(this.sopas.length!=this.sopas[i].length){
                rta = false;
            }
        }

        return rta;
    }
    
    
    public boolean esDispersa() 
    {
        if(getSopas()==null){return false;}
        boolean rta=!esCuadrada()&&!esRectangular();
        return rta;
    }
    
     public boolean esRectangular() 
    {
        boolean esRectangular =true;
        if(getSopas()==null){return false;}
        for(int i = 0; i<this.sopas.length-1 && esRectangular; i++){
            if((this.sopas[i].length!=this.sopas[i+1].length) || esCuadrada()){
                esRectangular=false;
            }
        }

        return esRectangular;
    }
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
        if(!esCuadrada() || this.sopas == null){
            return null;
        }
        char diagonal[] = new char[this.sopas.length];
        for(int i = 0; i<this.sopas.length; i++){
            diagonal[i] = this.sopas[i][i];
        }
        return  diagonal;
    }

    public String getImprimir() {
        return imprimir;
    }

    public void setImprimir(String imprimir) {
        this.imprimir = this.imprimir+imprimir+"\n";
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopa
     * @return s*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!

    
}
